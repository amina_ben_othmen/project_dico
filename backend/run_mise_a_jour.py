# Ce script permet de classer la mise a jour du dico
# il prend en entrée un fichier Excel contient de nouvelles phrases ainsi que leurs codes dico
# et les classer dans les dataset


import pandas as pd
from joblib import load,dump
import pickle
from backend.cleaning2 import clean_text, sentence_preprocessing, data_preprocessing2
from fuzzywuzzy import fuzz
from sklearn.ensemble import RandomForestClassifier
from backend.cross_validation2 import cross_validation
from sklearn.model_selection import train_test_split
df_maj = pd.read_excel('df_maj_avec_code.xlsx')
df_min = pd.read_excel('df_min_avec_code.xlsx')
df_delta = pd.read_excel('ech.xlsx')
clf_maj = load('best_model_maj.joblib')
clf_min = load('best_model_min.joblib')
#histo = pd.read_excel('historique_a_remplire')
with open('donnees', 'rb') as fichier :
     mon_depickler = pickle.Unpickler(fichier)
     cv_maj = mon_depickler.load()
with open('donnees_best', 'rb') as fichier2 :
     mon_depickler2 = pickle.Unpickler(fichier2)
     cv_min = mon_depickler2.load()

def trouver_phrase (df,i):
    k = 0
    pos = 0
    ok = False
    for j in df['phraes_similaires']:
        if i == j :
            ok = True
            pos = k
            break
        k = k+1
    return ok ,pos

def len_classe(df,nom):
    compteur = 0
    for i in df['nom_classe']:
        if i == nom :
            compteur = compteur +1
    return compteur

def placer_dans_maj (df_min,df_maj):
    l_noms = []
    for i in df_min['nom_classe']:
        if i not in l_noms:
            l_noms.append(i)
    listeplus13= []
    for i in l_noms:
        if len_classe(df_min, i) >= 13:
            listeplus13.append(i)

    len_min = len(df_min.index)
    len_maj = len(df_maj.index)
    n = 0
    for i in df_min['nom_classe']:

        if i in listeplus13:

            df_maj.loc[len_maj] = ['none',df_min['code_dico'][n], df_min['phraes_similaires'][n], i]
            df_maj.reset_index(drop=True, inplace=True)
            len_maj = len_maj +1
            df_min.drop([n], inplace=True)

        n = n+1


def mise_a_jour(df_delta, clf_maj, df_maj, cv_maj,clf_min, df_min, cv_min):
    f = 0
    for i in df_delta['libelle']:

        ok_maj , k_maj = trouver_phrase(df_maj,i)
        print (ok_maj,k_maj)
        if ok_maj :
            df_maj.loc[k_maj] = [df_maj['num_classe'][k_maj], df_delta['code_dico'][f], i, df_maj['nom_classe'][k_maj]]

        ok_min, k_min = trouver_phrase(df_min, i)
        print(ok_min, k_min)
        if ok_min :
            df_min.loc[k_min] = [df_delta['code_dico'][f], i, df_min['nom_classe'][k_min]]

        if not ok_maj and not ok_min :
            print ("if not")
            modif = False
            cleaned = clean_text(i)
            cleaned_vec = [cleaned]
            for j in df_maj['phraes_similaires']:
                cleaned_j = clean_text(j)
                score = fuzz.ratio(cleaned, cleaned_j)
                if score >= 60:
                    break
            if score >= 60:
                vect = sentence_preprocessing(cleaned_vec, cv_maj)
                prob = clf_maj.predict_proba(vect)
                if prob.max() >= 0.7:
                    my_prediction = clf_maj.predict(vect)
                    str1 = ''.join([str(i) for i in my_prediction])
                    df_maj.reset_index(drop=True, inplace=True)
                    fo = len(df_maj.index)
                    df_maj.loc[fo] = ['none', df_delta['code_dico'][f], i, str1]
                    df_maj.reset_index(drop=True, inplace=True)
                    modif = True

            if not modif:
                for kj in df_min['phraes_similaires']:
                    cleaned_kj = clean_text(kj)
                    score2 = fuzz.ratio(cleaned, cleaned_kj)
                    if score2 >= 60:
                        break
                if score2 >= 60:
                    vect2 = sentence_preprocessing(cleaned_vec, cv_min)
                    prob2 = clf_min.predict_proba(vect2)
                    if prob2.max() >= 0.7:
                        my_prediction2 = clf_min.predict(vect2)
                        str2 = ''.join([str(i) for i in my_prediction2])
                        df_min.reset_index(drop=True, inplace=True)
                        df_min.loc[len(df_min.index)] = [df_delta['code_dico'][f], i, str2]
                        df_min.reset_index(drop=True, inplace=True)

                    else:

                        print(len(df_min.index))
                        df_min.reset_index(drop=True, inplace=True)
                        df_min.loc[len(df_min.index)] = [df_delta['code_dico'][f], i, str(f)]
                        df_min.reset_index(drop=True, inplace=True)
                        print('dans else ', i)

            placer_dans_maj(df_min, df_maj)
            df_min.reset_index(drop=True, inplace=True)
            df_maj.reset_index(drop=True, inplace=True)
            # ----------------------------------min-------------------------------------------------------------
            clf_min = RandomForestClassifier(n_estimators=8, criterion='gini')
            X_min = df_min['phraes_similaires']
            y_min = df_min['nom_classe']
            X_min = X_min.apply(clean_text)
            # df_min['phraes_similaires'] = df_min['phraes_similaires'].apply(clean_text)
            X2_min, cv_min = data_preprocessing2(df_min, X_min)
            X_train_min, X_test_min, y_train_min, y_test_min = train_test_split(X2_min, y_min, train_size=0.8,
                                                                                random_state=0)
            clf_min.fit(X_train_min, y_train_min)
            """with open('donnees_best', 'wb') as fichier:
                mon_pickler1 = pickle.Pickler(fichier)
                mon_pickler1.dump(cv_min)
            
            dump(clf_min, 'best_model_min.joblib')"""
            # -------------------------------------maj------------------------------------------------
            clf_maj = RandomForestClassifier(n_estimators=8, criterion='gini')
            X_maj = df_maj['phraes_similaires']
            y_maj = df_maj['nom_classe']
            X_maj = X_maj.apply(clean_text)
            # df_maj['phraes_similaires'] = df_maj['phraes_similaires'].apply(clean_text)
            X2_maj, cv_maj = data_preprocessing2(df_maj, X_maj)
            X_train_maj, y_train_maj, X_test_maj, y_test_maj = cross_validation(X2_maj, y_maj)
            clf_maj.fit(X_train_maj, y_train_maj)
            """with open('donnees', 'wb') as fichier_best:
                mon_pickler = pickle.Pickler(fichier_best)
                mon_pickler.dump(cv_maj)
            
            dump(clf_maj, 'best_model_maj.joblib')"""
        f = f + 1
    df_min.to_excel("df_min_avec_code1.xlsx", index=False)
    df_maj.to_excel("df_maj_avec_code1.xlsx", index=False)

    """df_min.to_excel("df_min_avec_code.xlsx", index=False)
    df_maj.to_excel("df_maj_avec_code.xlsx", index=False)"""


mise_a_jour (df_delta, clf_maj, df_maj, cv_maj, clf_min, df_min, cv_min)