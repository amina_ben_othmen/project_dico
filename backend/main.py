"""Main module."""
import logging
from flask import Flask
from blueprint import DICO_API_BLUEPRINT



logging.basicConfig(level=logging.DEBUG)

# create flask server
app = Flask(__name__)  # pylint:disable=invalid-name
app.register_blueprint(DICO_API_BLUEPRINT, url_prefix="/api/dico")

if __name__ == "__main__":
    app.run(threaded=True, port=8081, debug=True)
