#!/bin/bash

# can't use '-u' flags cause of sourcing python env
set -eo pipefail
cd "$(dirname "${BASH_SOURCE}")"

rm -rf env
virtualenv -p python3 env

source env/bin/activate

pip3 install -r requirements.txt
