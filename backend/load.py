from joblib import load
import pickle
import xlwt
import pandas as pd
from classifier_dico import classer_dico_total


df_maj = pd.read_excel('df_maj_avec_code.xlsx')
df_min = pd.read_excel('df_min_avec_code.xlsx')
df_delta = pd.read_excel('ech.xlsx')
clf_maj = load('best_model_maj.joblib')
clf_min = load('best_model_min.joblib')
#histo = pd.read_excel('historique_a_remplire')
with open('donnees', 'rb') as fichier :
     mon_depickler = pickle.Unpickler(fichier)
     cv_maj = mon_depickler.load()
with open('donnees_best', 'rb') as fichier2 :
     mon_depickler2 = pickle.Unpickler(fichier2)
     cv_min = mon_depickler2.load()



#______________________________classer_totalité_dico_________________________________________________________________
def classer_dico(i):
     return classer_dico_total(i, clf_maj, df_maj, cv_maj, clf_min, df_min, cv_min)

