import logging
import pandas as pd
from flask import Blueprint, request, jsonify
from load import classer_dico
from classifier_dico import add_histo, non_trouv
from .blueprint_utils import flask_check_and_inject_payload
#histo = pd.read_excel('historique_a_remplire.xlsx')

with open("historic.xml", "r", encoding="utf8") as FIn:
    lines = FIn.readlines()
FIn.close()

df_maj = pd.read_excel('df_maj_avec_code.xlsx')
df_min = pd.read_excel('df_min_avec_code.xlsx')

DICO_API_BLUEPRINT = Blueprint("dico_blueprint", __name__)

@DICO_API_BLUEPRINT.route("/search", methods=["GET"])
def search():
    message = request.args.get("message")
    logging.info("Message : %s", message)

    liste = classer_dico(message)

    return jsonify(liste), 200

@DICO_API_BLUEPRINT.route("/add", methods=["PUT"])
@flask_check_and_inject_payload()
def add(payload):
    code = payload["code"] if "code" in payload else None
    phrase_ch = payload["phrasech"] if "phrasech" in payload else ""
    #pourcentage = payload["pourcentage"] if "pourcentage" in payload else None
    classe = payload["nom_classe"] if "nom_classe" in payload else None

    logging.info("Dico : %s", payload)

    if code and phrase_ch and classe:
        logging.info("add function")
        add_histo(phrase_ch, code,classe, lines, df_min, df_maj)
    elif phrase_ch and not code and not classe:
        logging.info("not found function")
        non_trouv(phrase_ch, df_min)
    else:
        logging.error("error données")

    return jsonify({'msg': 'OK'}), 200
