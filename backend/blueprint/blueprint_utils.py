import json
from collections import OrderedDict
from cerberus import Validator
from flask import jsonify, request
from functools import wraps

def flask_check_and_inject_payload(validation_schema=None):
    """
        Args:
            validation_schema (dict):

        Returns:
            (funct):
    """

    def decorated(funct):
        @wraps(funct)
        def wrapper(*args, **kwargs):
            if request.headers.get("Content-Type") in ["application/json"]:
                try:
                    payload_dict = json.loads(
                        request.data,
                        object_pairs_hook=OrderedDict,
                        encoding="utf8"
                    )
                except ValueError as err:
                    return jsonify(err), 422

                if validation_schema:
                    validator = Validator(validation_schema)
                    # Check if the document is valid.
                    if not validator.validate(payload_dict):
                        return jsonify("Wrong args."), 422
                kwargs["payload"] = payload_dict
                return funct(*args, **kwargs)
            return jsonify("The payload format is unknown"), 422
        return wrapper
    return decorated
