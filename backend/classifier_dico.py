# Ce script permet de faire la recherche des phrases dans les dataset
# ainsi il permet d'ajouter la phrase choisie dans l'historique et dans le dataset
# lorsque le bouton aucune approximation trouvée est cliqué la fonction non_trouve s'execute


import xlwt
from joblib import dump
from operator import itemgetter
from cleaning2 import clean_text,sentence_preprocessing,cosine_similarity,data_preprocessing2
from fuzzywuzzy import fuzz
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from cross_validation2 import cross_validation
#from sklearn.model_selection import train_test_split
import pickle
from openpyxl import Workbook, load_workbook
#from knn_minoritaire import knn_minoritaires
from pyrxnlp.api.text_similarity import TextSimilarity

def get_label_nom_classe (nom_classe,df):
    len_ = len(df.index)
    i = 0
    ok = False
    while i <= len_ and ok == False :
        if df['nom_classe'][i] == nom_classe :
            ok = True
        i = i+1

    return i



def get_label_phrase (phrase,df):
    len = len(df.index)
    i = 0
    ok = False
    while i <= len and ok == False :
        if df['phraes_similaires'][i] == phrase :
            ok = True
        i = i+1

    return i



def len_classe(df,nom):
    compteur = 0
    for i in df['nom_classe']:
        if i == nom :
            compteur = compteur +1
    return compteur

def placer_dans_maj (df_min,df_maj):
    l_noms = []
    for i in df_min['nom_classe']:
        if i not in l_noms:
            l_noms.append(i)
    listeplus13= []
    for i in l_noms:
        if len_classe(df_min, i) >= 13:
            listeplus13.append(i)

    len_min = len(df_min.index)
    len_maj = len(df_maj.index)
    n = 0
    for i in df_min['nom_classe']:

        if i in listeplus13:

            df_maj.loc[len_maj] = ['none',df_min['code_dico'][n], df_min['phraes_similaires'][n], i]
            df_maj.reset_index(drop=True, inplace=True)
            len_maj = len_maj +1
            df_min.drop([n], inplace=True)

        n = n+1



def AddItem(MPMSyntax, Trad,lines1):

    lines1.pop()
   # print(lines)
    lines1.append("  <Request MPMSyntax=\"" + MPMSyntax + "\" OriginalText= \"" + Trad + "\"/>\n")
    lines1.append("</Requests>")

    with open("historic.xml", "w", encoding="utf8") as FOut:
        for line in lines1:
            FOut.write(line)
    FOut.close()

def classer_dico_total (i, clf_maj, df_maj, cv_maj,clf_min, df_min, cv_min):

    modif = False
    len_maj = len(df_maj.index)
    cleaned = clean_text(i)
    cleaned_vec = [cleaned]
    liste=[]
    for j in df_maj['phraes_similaires']:
        cleaned_j = clean_text(j)
        score = fuzz.ratio(cleaned, cleaned_j)
        if score >= 60:
            break

    if score >= 60:
        vect = sentence_preprocessing(cleaned_vec, cv_maj)
        prob = clf_maj.predict_proba(vect)
        print('prob',prob)
        if prob.max() >= 0.29:
            my_prediction = clf_maj.predict(vect)
            str1 = ''.join([str(i) for i in my_prediction])
            lk = 0

            for name in df_maj['nom_classe'] :
                if name == str1 and df_maj['code_dico'][lk]!= "None":
                    json_tmp = {
                        "code": df_maj['code_dico'][lk],
                        "phrase": df_maj['phraes_similaires'][lk],
                        "pourcentage": fuzz.ratio(df_maj['phraes_similaires'][lk],i),
                        "nom_classe": df_maj['nom_classe'][lk]
                    }
                    liste.append(json_tmp)
                lk = lk +1

            modif = True

    if modif == False:
        for kj in df_min['phraes_similaires']:
            cleaned_kj = clean_text(kj)
            score2 = fuzz.ratio(cleaned, cleaned_kj)
            if score2 >= 60:
                break

        if score2 >= 60:
            vect2 = sentence_preprocessing(cleaned_vec, cv_min)
            prob2 = clf_min.predict_proba(vect2)
            print ('prob2',prob2)
            if prob2.max() >= 0.3:
                my_prediction2 = clf_min.predict(vect2)
                print(i, my_prediction2)
                str2 = ''.join([str(i) for i in my_prediction2])
                lk = 0
                for nom in df_min['nom_classe']:
                    if nom == str2 and df_min['code_dico'][lk]!= "None" :
                        json_tmp = {
                            "code": df_min['code_dico'][lk],
                            "phrase": df_min['phraes_similaires'][lk],
                            "pourcentage": fuzz.ratio(df_min['phraes_similaires'][lk],i),
                            "nom_classe": df_min['nom_classe'][lk]
                        }
                        liste.append(json_tmp)
                    lk = lk + 1

        else:
            print ('none')

    print (liste)
    return sorted(liste, key=itemgetter('pourcentage'),reverse = True)


def non_trouv (ph_cherch,df_min) :
    df_min.reset_index(drop=True, inplace=True)
    df_min.loc[len(df_min.index)] = ['None', ph_cherch, str(len(df_min.index))]
    df_min.reset_index(drop=True, inplace=True)
    clf_min = RandomForestClassifier(criterion='gini')
    X_min = df_min['phraes_similaires']
    y_min = df_min['nom_classe']
    X_min = X_min.apply(clean_text)
    X2_min, cv_min = data_preprocessing2(df_min, X_min)
    with open('donnees_best', 'wb') as fichier:
        mon_pickler1 = pickle.Pickler(fichier)
        mon_pickler1.dump(cv_min)
    X_train_min, X_test_min, y_train_min, y_test_min = train_test_split(X2_min, y_min, train_size=0.8, random_state=0)
    clf_min.fit(X_train_min, y_train_min)
    dump(clf_min, 'best_model_min.joblib')
    df_min.to_excel("df_min_avec_code.xlsx",columns=['code_dico','phraes_similaires','nom_classe'])
    print('la phrase est ajouté et le modele est ré-entrainé')


def add_histo (ph_cherch, code_trouv,nom_classe, lines1, df_min, df_maj):
    l_noms_maj = []
    for i in df_maj['nom_classe']:
        if i not in l_noms_maj:
            l_noms_maj.append(i)

    if nom_classe in l_noms_maj :
        if ph_cherch not in df_maj['phraes_similaires']:
            df_maj.reset_index(drop=True, inplace=True)
            fo = len(df_maj.index)
            df_maj.loc[fo] = ['None','None', ph_cherch, nom_classe]
            df_maj.reset_index(drop=True, inplace=True)
            clf_maj = RandomForestClassifier(criterion='gini')
            X_maj = df_maj['phraes_similaires']
            y_maj = df_maj['nom_classe']
            X_maj = X_maj.apply(clean_text)
            X2_maj, cv_maj = data_preprocessing2(df_maj, X_maj)
            X_train_maj, y_train_maj, X_test_maj, y_test_maj = cross_validation(X2_maj, y_maj)
            clf_maj.fit(X_train_maj, y_train_maj)
            with open('donnees', 'wb') as fichier_best:
                mon_pickler = pickle.Pickler(fichier_best)
                mon_pickler.dump(cv_maj)

            dump(clf_maj, 'best_model_maj.joblib')
    else:
        if ph_cherch not in df_min['phraes_similaires']:
            df_min.reset_index(drop=True, inplace=True)
            df_min.loc[len(df_min.index)] = ['None', ph_cherch, nom_classe]
            df_min.reset_index(drop=True, inplace=True)
            placer_dans_maj(df_min, df_maj)
            df_min.reset_index(drop=True, inplace=True)
            df_maj.reset_index(drop=True, inplace=True)
            clf_min = RandomForestClassifier(criterion='gini')
            X_min = df_min['phraes_similaires']
            y_min = df_min['nom_classe']
            X_min = X_min.apply(clean_text)
            X2_min, cv_min = data_preprocessing2(df_min, X_min)
            with open('donnees_best', 'wb') as fichier:
                mon_pickler1 = pickle.Pickler(fichier)
                mon_pickler1.dump(cv_min)
            X_train_min, X_test_min, y_train_min, y_test_min = train_test_split(X2_min, y_min, train_size=0.8, random_state=0)
            clf_min.fit(X_train_min, y_train_min)
            dump(clf_min, 'best_model_min.joblib')

        #____________________enregistrer dans historique______________________
    AddItem(code_trouv,ph_cherch,lines1)
    #len_histo = len(df_maj.index)
    #histo.loc[len_histo] = [code_trouv, ph_cherch]
    #histo.reset_index(drop=True, inplace=True)
        #__________________________________ to excel __________________________________
    df_min.to_excel("df_min_avec_code.xlsx", index=False)
    df_maj.to_excel("df_maj_avec_code.xlsx", index=False)
    #histo.to_excel("historique_a_remplire.xlsx")

