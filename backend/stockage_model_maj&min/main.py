import pandas as pd
import numpy as np
from cleaning import clean_text, data_preprocessing
from RandomOverSampler import Random_Over_Sampler
from crossvalidation import cross_validation
from classification import best_model
from joblib import dump
import os
import pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split



def main():
    """df2 = pd.read_excel('dataset_majoritaire.xlsx')

    X = df2['phraes_similaires']
    y = df2['nom_classe']
    df2['phraes_similaires'] = df2['phraes_similaires'].apply(clean_text)
    #print(data_preprocessing(df2).head(12))
    X2,cv1 = data_preprocessing(df2, X, y)
    with open('donnees', 'wb') as fichier:
        mon_pickler = pickle.Pickler(fichier)
        mon_pickler.dump(cv1)

    X_res, y_res = Random_Over_Sampler(df2, X2, y)
    X_train, X_test, y_train, y_test = cross_validation(X2, y)
    clf = best_model(X_train, y_train, X_test, y_test)
    dump(clf, 'model.joblib')"""

    #__________________________train_minoritaire_______________________________________
    """print("minoritaire")
    df_minoritaire = pd.read_excel('dataset_minoritaire_moins_13.xlsx')
    X_min = df_minoritaire['phraes_similaires']
    y_min = df_minoritaire['nom_classe']
    df_minoritaire['phraes_similaires'] = df_minoritaire['phraes_similaires'].apply(clean_text)
    X2_min, cv_min_best = data_preprocessing(df_minoritaire, X_min, y_min)
    with open('donnees_best', 'wb') as fichier_best:
        mon_pickler5 = pickle.Pickler(fichier_best)
        mon_pickler5.dump(cv_min_best)
    #X_res_min, y_res_min = Random_Over_Sampler(df_minoritaire, X2_min, y_min)
    X_train_min, y_train_min, X_test_min, y_test_min = train_test_split(X2_min, y_min, train_size=0.8)

    #X_train_min, y_train_min, X_test_min, y_test_min = cross_validation(X2_min, y_min)
    clf_min = best_model(X_train_min, y_train_min, X_test_min, y_test_min)
    dump(clf_min, 'best_model_min.joblib')"""
 #__________________________train_minoritaire_rep_______________________________________
    """print("minoritaire_rep")
    df_minoritaire_rep = pd.read_excel('C://Users//abenothm\PycharmProjects//test_load//representatif_minoritaire.xlsx')
    X_min_rep = df_minoritaire_rep['phraes_similaires']
    y_min_rep = df_minoritaire_rep['nom_classe']
    df_minoritaire_rep['phraes_similaires'] = df_minoritaire_rep['phraes_similaires'].apply(clean_text)
    X2_min_rep, cv_min_rep = data_preprocessing(df_minoritaire_rep, X_min_rep, y_min_rep)
    with open('donnees3', 'wb') as fichier3:
        mon_pickler3 = pickle.Pickler(fichier3)
        mon_pickler3.dump(cv_min_rep)
    #X_res_min, y_res_min = Random_Over_Sampler(df_minoritaire, X2_min, y_min)
    X_train_min_rep, y_train_min_rep, X_test_min_rep, y_test_min_rep = cross_validation(X2_min_rep, y_min_rep)
    clf_min_rep = best_model(X_train_min_rep, y_train_min_rep, X_test_min_rep, y_test_min_rep)
    dump(clf_min_rep, 'model_min_rep.joblib')"""
#_________________________________________________________________________________________________________________
    """print("minoritaire")
    df_minoritaire = pd.read_excel('dataset_minoritaire_moins_13.xlsx')
    X_min = df_minoritaire['phraes_similaires']
    y_min = df_minoritaire['nom_classe']
    df_minoritaire['phraes_similaires'] = df_minoritaire['phraes_similaires'].apply(clean_text)
    X2_min, cv_min_best = data_preprocessing(df_minoritaire, X_min, y_min)
    with open('donnees_best', 'wb') as fichier_best:
        mon_pickler5 = pickle.Pickler(fichier_best)
        mon_pickler5.dump(cv_min_best)
    #X_res_min, y_res_min = Random_Over_Sampler(df_minoritaire, X2_min, y_min)
    X_train_min,X_test_min, y_train_min, y_test_min = train_test_split(X2_min, y_min, train_size=0.8)
    #X_train_min, y_train_min, X_test_min, y_test_min = cross_validation(X2_min, y_min)
    clf_min = best_model(X_train_min, X_test_min, y_train_min,  y_test_min)
    dump(clf_min, 'best_model_min.joblib')"""

    df_minoritaire = pd.read_excel('dataset_minoritaire_moins_13.xlsx')

    X_min = df_minoritaire['phraes_similaires']
    y_min = df_minoritaire['nom_classe']
    df_minoritaire['phraes_similaires'] = df_minoritaire['phraes_similaires'].apply(clean_text)
    X2_min, cv_min_best = data_preprocessing(df_minoritaire, X_min, y_min)
    with open('donnees_best', 'wb') as fichier_best:
        mon_pickler5 = pickle.Pickler(fichier_best)
        mon_pickler5.dump(cv_min_best)

    X_train_min, X_test_min, y_train_min, y_test_min = train_test_split(X2_min, y_min, train_size=0.8)
    Random_Forest_Classifier = RandomForestClassifier(criterion='gini', class_weight='balanced')
    Random_Forest_Classifier.fit(X_train_min, y_train_min)
    #y_pred = Random_Forest_Classifier.predict(X_test)
    dump(Random_Forest_Classifier, 'best_model_min.joblib')

if __name__ == '__main__':
    main()
