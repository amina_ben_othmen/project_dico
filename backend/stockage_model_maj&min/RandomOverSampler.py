from imblearn.over_sampling import SMOTE, RandomOverSampler,ADASYN
from imblearn.pipeline import make_pipeline
from imblearn.combine import SMOTEENN, SMOTETomek


def Random_Over_Sampler(df2, X, y):
    e = []
    for i in df2['nom_classe']:
        if i not in e:
            print("Before OverSampling, counts of label", i, sum(y == i))
            e.append(i)

    #__________________smote_randomoversampler_________________________________________________________________________
    sm = SMOTE()
    ros = RandomOverSampler()
    my_pipe = make_pipeline(ros, sm)
    X_res, y_res = my_pipe.fit_resample(X, y)
    #__________________________________________________________________________________________________________________

    #___________________________SMOTEENN_______________________________________________________________________________
    """sme = SMOTEENN(random_state=42)
    X_res, y_res = sme.fit_resample(X, y)"""

    # ______________________________SMOTETomek__________________________________________________________
    """smt = SMOTETomek(random_state=42)
    X_res, y_res = smt.fit_resample(X, y)"""

    print('After OverSampling, the shape of train_X: {}'.format(X_res.shape))
    print('After OverSampling, the shape of train_y: {} \n'.format(y_res.shape))
    e2 = []
    for i in df2['nom_classe']:
        if i not in e2:
            print("After OverSampling, counts of label", i, sum(y_res == i))
            e2.append(i)

    return X_res, y_res