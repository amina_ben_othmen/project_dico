from sklearn.model_selection import StratifiedKFold


def cross_validation(X_res, y_res):
    skf = StratifiedKFold(n_splits=2, shuffle=True)
    skf.get_n_splits(X_res, y_res)
    for train_index, test_index in skf.split(X_res, y_res):
        print("TRAIN:", train_index, "TEST:", test_index)
        X_train, X_test = X_res[train_index], X_res[test_index]
        y_train, y_test = y_res[train_index], y_res[test_index]
    return X_train, y_train, X_test, y_test
