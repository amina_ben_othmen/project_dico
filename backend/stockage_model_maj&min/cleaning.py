from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
import pandas as pd
import re
import nltk
import string
nltk.download('stopwords')
nltk.download('punkt')
import os

def clean_text(text):
    sw = set()
    sw.update(tuple(stopwords.words('french')))
    text = str(text).lower()  # lowercase text
    text = re.sub(r'\d+', '', text)
    # text = REPLACE_BY_SPACE_RE.sub(' ', text) # replace REPLACE_BY_SPACE_RE symbols by space in text
    tokenizer = nltk.RegexpTokenizer(r'\w+')
    text = tokenizer.tokenize(text)
    text = ' '.join(word for word in text if word not in sw)  # delete stopwors from text

    return text
def data_preprocessing(df2, X, y):
    df2.drop_duplicates(subset="phraes_similaires", keep="first", inplace=True)
    #X = df2['phraes_similaires']
    #y = df2['nom_classe']
    cv = TfidfVectorizer()
    X1 = cv.fit_transform(X)
    features_names = cv.get_feature_names()
    dense = X1.todense()
    dense_list = dense.tolist()
    df3 = pd.DataFrame(dense_list, columns=features_names)
    print (df3)

    return X1,cv

