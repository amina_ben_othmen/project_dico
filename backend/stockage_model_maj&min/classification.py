from sklearn import svm
from sklearn.svm import LinearSVC
from sklearn.metrics import confusion_matrix, f1_score, classification_report, confusion_matrix, accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder
import warnings

#from xgboost import XGBClassifier
def find_key(v, modeles):
    for k, val in modeles.items():
        if v == val:
            return k


def best_model(X_train, X_test, y_train, y_test):
    Random_Forest_Classifier = RandomForestClassifier(n_estimators=8, criterion='gini',class_weight='balanced')
    KNeighbors_Classifier = KNeighborsClassifier(n_neighbors=5, p=1)
    LinearSVC_ = SVC(kernel= 'linear', probability = True)
    Logistic_Regression = LogisticRegression()
    svc = SVC(decision_function_shape='ovo')
    # xg_reg = XGBClassifier()
    modeles = dict()

    liste1 = [Random_Forest_Classifier,KNeighbors_Classifier, LinearSVC_, Logistic_Regression, svc]
    for i in liste1:
        i.fit(X_train, y_train)
        y_pred = i.predict(X_test)
        #cm = confusion_matrix(y_test, y_pred)
        accuracy = accuracy_score(y_test, y_pred)
        k = accuracy * 100.0
        modeles[i] = k
        warnings.filterwarnings('ignore')
        print(classification_report(y_test, y_pred))
    print("best model is :{} , with accuracy: {}".format(find_key(max(modeles.values()), modeles), max(modeles.values())))
    print (modeles)
    return find_key(max(modeles.values()), modeles)
