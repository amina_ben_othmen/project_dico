# Ce script consiste à classer le dico total après l'avoir divisé en plusieurs parties
# car le fichier dictionnaire est trop volumineux


from cleaning2 import clean_text, sentence_preprocessing, data_preprocessing2
from fuzzywuzzy import fuzz
from sklearn.ensemble import RandomForestClassifier
from cross_validation2 import cross_validation
from sklearn.model_selection import train_test_split
from joblib import load,dump
import pickle
import pandas as pd

df_maj = pd.read_excel('df_maj_avec_code.xlsx')
df_min = pd.read_excel('df_min_avec_code.xlsx')
clf_maj = load('C://Users//amina//PycharmProjects//first_test--//best_model_maj.joblib')
clf_min = load('C://Users//amina//PycharmProjects//first_test--//best_model_min.joblib')
#histo = pd.read_excel('historique_a_remplire')
with open('C://Users//amina//PycharmProjects//first_test--//donnees', 'rb') as fichier :
     mon_depickler = pickle.Unpickler(fichier)
     cv_maj = mon_depickler.load()
with open('C://Users//amina//PycharmProjects//first_test--//donnees_best', 'rb') as fichier2 :
     mon_depickler2 = pickle.Unpickler(fichier2)
     cv_min = mon_depickler2.load()

def get_label_nom_classe(nom_classe, df):
    len_ = len(df.index)
    i = 0
    ok = False
    while i <= len_ and ok == False :
        if df['nom_classe'][i] == nom_classe :
            ok = True
        i = i+1

    return i


def get_label_phrase (phrase,df):
    len = len(df.index)
    i = 0
    ok = False
    while i <= len and ok == False :
        if df['phraes_similaires'][i] == phrase :
            ok = True
        i = i+1

    return i


def len_classe(df,nom):
    compteur = 0
    for i in df['nom_classe']:
        if i == nom:
            compteur = compteur +1
    return compteur


def placer_dans_maj (df_min, df_maj):
    l_noms = []
    for i in df_min['nom_classe']:
        if i not in l_noms:
            l_noms.append(i)
    listeplus13 = []
    for i in l_noms:
        if len_classe(df_min, i) >= 13:
            listeplus13.append(i)

    len_min = len(df_min.index)
    len_maj = len(df_maj.index)
    n = 0
    for i in df_min['nom_classe']:

        if i in listeplus13:

            #df_maj.reset_index(drop=True, inplace=True)
            df_maj.loc[len_maj] = ['none', df_min['code_dico'][n], df_min['phraes_similaires'][n], i]
            df_maj.reset_index(drop=True, inplace=True)
            len_maj = len_maj + 1
            df_min.drop([n], inplace=True)
            #df_min.reset_index(drop=True, inplace=True)

        n = n+1


def classer_partie_dico(dico, clf_maj, df_maj, cv_maj,clf_min, df_min, cv_min,nb):

    f = 0
    modif = False
    for i in dico['libelle']:
        modif = False
        score = 0
        cleaned = clean_text(i)
        cleaned_vec = [cleaned]
        for j in df_maj['phraes_similaires']:
            score = fuzz.ratio(i, j)
            if score >= 60:
                break

        if score >= 60:
            vect = sentence_preprocessing(cleaned_vec, cv_maj)
            prob = clf_maj.predict_proba(vect)
            if prob.max() >= 0.7:
                my_prediction = clf_maj.predict(vect)
                str1 = ''.join([str(i) for i in my_prediction])
                df_maj.reset_index(drop=True, inplace=True)
                fo = len(df_maj.index)
                df_maj.loc[fo] = ['none', dico['code_dico'][f], i, str1]
                df_maj.reset_index(drop=True, inplace=True)
                modif = True


        if not modif:

            score2 = 0
            for kj in df_min['phraes_similaires'] :
                score2 = fuzz.ratio(i, kj)
                if score2 >= 60:
                    break

            if score2 >= 60:
                vect2 = sentence_preprocessing(cleaned_vec, cv_min)
                prob2 = clf_min.predict_proba(vect2)
                if prob2.max() >= 0.7:
                    my_prediction2 = clf_min.predict(vect2)
                    str2 = ''.join([str(i) for i in my_prediction2])
                    df_min.reset_index(drop=True, inplace=True)
                    df_min.loc[len(df_min.index)] = [dico['code_dico'][f], i, str2]
                    df_min.reset_index(drop=True, inplace=True)

            else:

                df_min.reset_index(drop=True, inplace=True)
                df_min.loc[len(df_min.index)] = [dico['code_dico'][f], i, str(f+nb)]
                df_min.reset_index(drop=True, inplace=True)
        placer_dans_maj(df_min, df_maj)
        df_min.reset_index(drop=True, inplace=True)
        df_maj.reset_index(drop=True, inplace=True)
        # ----------------------------------re_entrainer_le_model_min-------------------------------------------------------------
        clf_min = RandomForestClassifier(n_estimators=8, criterion='gini')
        X_min = df_min['phraes_similaires']
        y_min = df_min['nom_classe']
        X_min = X_min.apply(clean_text)
        # df_min['phraes_similaires'] = df_min['phraes_similaires'].apply(clean_text)
        X2_min, cv_min = data_preprocessing2(df_min, X_min)
        X_train_min, X_test_min, y_train_min, y_test_min = train_test_split(X2_min, y_min, train_size=0.8, random_state=0)
        clf_min.fit(X_train_min, y_train_min)
        """with open('donnees_best', 'wb') as fichier:
                        mon_pickler1 = pickle.Pickler(fichier)
                        mon_pickler1.dump(cv_min)

        dump(clf_min, 'best_model_min.joblib')"""

        # ------------------------------------re_entrainer_le_model-maj------------------------------------------------
        clf_maj = RandomForestClassifier(n_estimators=8, criterion='gini')
        X_maj = df_maj['phraes_similaires']
        y_maj = df_maj['nom_classe']
        X_maj = X_maj.apply(clean_text)
        X2_maj, cv_maj = data_preprocessing2(df_maj, X_maj)
        X_train_maj, y_train_maj, X_test_maj, y_test_maj = cross_validation(X2_maj, y_maj)
        clf_maj.fit(X_train_maj, y_train_maj)
        """with open('donnees', 'wb') as fichier_best:
            mon_pickler = pickle.Pickler(fichier_best)
            mon_pickler.dump(cv_maj)

        dump(clf_maj, 'best_model_maj.joblib')"""
        f = f + 1

    df_min.to_excel("df_min_avec_code5.xlsx", index=False)
    df_maj.to_excel("df_maj_avec_code5.xlsx", index=False)
    """df_min.to_excel("df_min_avec_code.xlsx", index=False)
    df_maj.to_excel("df_maj_avec_code.xlsx", index=False)"""

    return f+nb


dico = pd.read_excel('C://aminanew//dico//backend//partitions dico//0.xlsx')
dico2 = pd.read_excel('C://aminanew//dico//backend//partitions dico//1.xlsx')
dico3 = pd.read_excel('C://aminanew//dico//backend//partitions dico//2.xlsx')
dico4 = pd.read_excel('C://aminanew//dico//backend//partitions dico//3.xlsx')
dico5 = pd.read_excel('C://aminanew//dico//backend//partitions dico//4.xlsx')
dico6 = pd.read_excel('C://aminanew//dico//backend//partitions dico//5.xlsx')
liste = [dico,dico2,dico3,dico4,dico5,dico6]
nb = 0
for i in liste:
    nb = classer_partie_dico(i,clf_maj, df_maj, cv_maj,clf_min, df_min, cv_min,nb)

# ____________________________________________ to excel____________________________________________________________

