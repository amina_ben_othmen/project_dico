import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmBoxComponent } from './confirm-box.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MaterialModule } from '../material/material.module';

describe('ConfirmBoxComponent', () => {
  let component: ConfirmBoxComponent;
  let fixture: ComponentFixture<ConfirmBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmBoxComponent ],
      imports: [MaterialModule],
      providers: [
        { provide: MatDialog, useValue: {open: () => {}} },
        { provide: MatDialogRef }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
