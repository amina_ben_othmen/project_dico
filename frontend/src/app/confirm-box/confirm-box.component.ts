import { Component, Inject, Optional, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-confirm-box',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './confirm-box.component.html',
  styleUrls: ['./confirm-box.component.css']
})
export class ConfirmBoxComponent {
  action: string;
  localData: any;

  constructor(
    public dialogRef: MatDialogRef<ConfirmBoxComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {

    this.localData = {...data};
    this.action = this.localData.action;
  }

  doAction() {
    this.dialogRef.close({event: this.action, data: this.localData});
  }

  closeDialog() {
    this.dialogRef.close({event: 'Cancel'});
  }

}
