export interface Dico {
    code: string;
    phrase: string;
    pourcentage: string;
    phrasech?: string;
}
