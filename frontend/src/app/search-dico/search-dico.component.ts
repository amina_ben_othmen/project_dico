import { Component, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Dico } from './dico.type';
import { ConfirmBoxComponent } from '../confirm-box/confirm-box.component';
import { AlertBoxComponent } from '../alert-box/alert-box.component';
import { DicoService } from '../service/dico.service';
import { NgForm } from '@angular/forms';
import { ClipboardService } from 'ngx-clipboard';

@Component({
  selector: 'app-search-dico',
  templateUrl: './search-dico.component.html',
  styleUrls: ['./search-dico.component.css']
})
export class SearchDicoComponent {
  message: string;
  notfound = true;

  @ViewChild('dicoForm', {static: true}) dicoForm: NgForm;

  constructor(private dicoService: DicoService,
              public dialog: MatDialog,
              private clipboardService: ClipboardService) { }

  displayedColumns: string[] = ['code_dico', 'phrase', 'pourcentage_de_ressemblance', 'ajout'];

  dataSource = new MatTableDataSource<Dico>([]);

  search() {
    this.dicoService.getResults(this.message).subscribe((res: Dico[]) => {
      this.dataSource.data = res;
      this.notfound = false;

      if(this.dataSource.data.length == 0)
      {const dialogRef = this.dialog.open(AlertBoxComponent, {
      width: '500px',
    }); };

    });

  }

  confirmAdd(obj) {
    obj.action = 'confirmer l\'ajout de';
    obj.phrasech = this.message;
    const dialogRef = this.dialog.open(ConfirmBoxComponent, {
      width: '500px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event === 'confirmer l\'ajout de') {
        this.addPhrase(result.data);
      }
    });
  }

  addPhrase(data) {
    this.dicoForm.reset();
    this.dataSource.data = null;
    this.dicoService.addPhrase(data).subscribe();
    this.notfound = true;
  }

  notFound() {
    const obj = {phrase: this.message};
    this.confirmAdd(obj);
  }

  copy(value: string) {
    this.clipboardService.copy(value);
  }
}

