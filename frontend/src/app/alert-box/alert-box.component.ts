import { Component, Inject, Optional, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-alert-box',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './alert-box.component.html',
  styleUrls: ['./alert-box.component.css']
})
export class AlertBoxComponent {


  constructor(
    public dialogRef: MatDialogRef<AlertBoxComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {


  }


}
