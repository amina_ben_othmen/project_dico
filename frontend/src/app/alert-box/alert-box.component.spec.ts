import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmBoxComponent } from './alert-box.component';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MaterialModule } from '../material/material.module';

describe('AlertBoxComponent', () => {
  let component: AlertBoxComponent;
  let fixture: ComponentFixture<AlertBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertBoxComponent ],
      imports: [MaterialModule],
      providers: [
        { provide: MatDialog, useValue: {open: () => {}} },
        { provide: MatDialogRef }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
