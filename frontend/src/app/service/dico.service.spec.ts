import { TestBed } from '@angular/core/testing';

import { DicoService } from './dico.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('DicoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      DicoService,
      HttpClient,
      HttpHandler
    ]
  }));

  it('should be created', () => {
    const service: DicoService = TestBed.get(DicoService);
    expect(service).toBeTruthy();
  });
});
