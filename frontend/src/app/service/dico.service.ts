import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dico } from '../search-dico/dico.type';

@Injectable({
  providedIn: 'root'
})
export class DicoService {
  private apiUrl = '/api/dico/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) {
  }

  getResults(msg: string): Observable<Dico[]> {
    return this.http.get<Dico[]>(`${this.apiUrl}` + 'search', {params: {message: msg}});
  }

  addPhrase(data: Dico): Observable<Dico> {
    return this.http.put<Dico>(`${this.apiUrl + 'add'}`, data, this.httpOptions);
  }
}
