import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SearchDicoComponent } from './search-dico/search-dico.component';
import {MaterialModule} from './material/material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule, MatInputModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { ConfirmBoxComponent } from './confirm-box/confirm-box.component';
import { AlertBoxComponent } from './alert-box/alert-box.component';
import { ClipboardModule } from 'ngx-clipboard';

@NgModule({
  declarations: [
    AppComponent,
    SearchDicoComponent,
    HeaderComponent,
    ContentComponent,
    ConfirmBoxComponent,
    AlertBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ClipboardModule
  ],
  providers: [],
  entryComponents: [
    ConfirmBoxComponent,
    AlertBoxComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
