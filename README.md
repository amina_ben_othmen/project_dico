## Les étapes de lancer l'application : 

le projet est sous le tmp avec cette adresse `https://budiag3.actia.fr/4120/script_generation_applicatif/trunk/Machine_learning_PFE`

 *  Installer node.js https://nodejs.org/en/download/.
 *  Installer angular CLI avec la commande `npm install -g @angular/cli`
 *  executer la commande `ng new project`
    * Ecraser src et la remplacer par le dossier src de notre projet 
 *  Installer C++ Build Tools de visual studio
 *  Exécutez le fichier launch.bat:
 
    * Il installera toutes les dépendances python nécessaires au projet.
    * Il contient la commande `npm start` pour lancer l'interface web et accéder à `http: // localhost: 8080 /`. L'application se rechargera automatiquement.
    * Il contient la commande `python main.py` pour lancer le serveur et la partie backend.  
  
## La fonction des principals scripts dans le projet: 
 * classifier_dico :  permet de faire la recherche des phrases dans les dataset ainsi il permet d'ajouter la phrase choisie dans l'historique et dans le dataset
. lorsque le bouton aucune approximation trouvée est cliqué la fonction non_trouve s'execute
 * run_mise_a_jour : permet de classer la mise a jour du dico , il prend en entrée un fichier Excel contient de nouvelles phrases ainsi que leurs codes dico
et les classer dans les dataset
 * classifier_dico_total : permet de classer le dico total après l'avoir divisé en plusieurs parties
 * cleaning2 : permet de faire le nettoyage des phrase
 * cross_validation : permet une meilleur division des data en train et test 
 
 
